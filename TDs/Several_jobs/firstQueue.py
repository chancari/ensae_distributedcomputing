import class1
import multiprocessing


def worker(q):
    obj = q.get()
    obj.do_something()


if __name__ == '__main__':
    queue = multiprocessing.Queue()

    p = multiprocessing.Process(target=worker, args=(queue,))
    p.start()

    queue.put(class1.OneClass('My object'))

    # Wait for the worker to finish
    queue.close()
    queue.join_thread()
    p.join()
