import multiprocessing
import os

def do_calculation(data):
    print('Called with process id:', os.getpid())
    return data * 2

def start_process():
    print( 'Starting', multiprocessing.current_process().name)
    print('process id:', os.getpid())

if __name__ == '__main__':
    inputs = list(range(16))
    print( 'Input   :', inputs)

    builtin_outputs = map(do_calculation, inputs)
    print( 'Built-in:', builtin_outputs)

    pool_size = multiprocessing.cpu_count() * 2
    pool = multiprocessing.Pool(processes=pool_size,
                                initializer=start_process,
                                maxtasksperchild=2,
                                )
    pool_outputs = pool.map(do_calculation, inputs)
    pool.close() # no more tasks
    pool.join()  # wrap up current tasks

    print( 'Pool    :', pool_outputs)
