import multiprocessing
from multiprocessing import Process
import time


def worker():
    name = multiprocessing.current_process().name
    print(name, 'Starting')
    time.sleep(2)
    print(name, 'Exiting')

def my_service():
    name = multiprocessing.current_process().name
    print(name, 'Starting')
    time.sleep(5)
    print(name, 'Exiting')

if __name__ == '__main__':
    service = multiprocessing.Process(name='my_service', target=my_service)
    worker_1 = Process(name='worker 1', target=worker)
    worker_2 = Process(target=worker) # use default name

    worker_1.start()
    worker_2.start()
    service.start()
