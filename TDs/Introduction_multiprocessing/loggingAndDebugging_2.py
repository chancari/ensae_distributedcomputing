import multiprocessing
import logging
import sys

def g():
    print('Arguments', g.__name__)
    sys.stdout.flush()

if __name__ == '__main__':
    multiprocessing.log_to_stderr()
    logger = multiprocessing.get_logger()
    logger.setLevel(logging.INFO)
    p = multiprocessing.Process(target=g)
    p.start()
    p.join()
