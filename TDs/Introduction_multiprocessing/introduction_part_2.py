from multiprocessing import Process
import os

def info(title):
    print(title)
    print('module name:', __name__)
    print('parent process:', os.getppid())
    print('process id:', os.getpid())

def f(name):
    info('function f')
    print('Arguments', name)

if __name__ == '__main__':
    jobs = []
    for i in range(5):
        p = Process(target=f, args=(i,))
        jobs.append(p)
        p.start()
        p.join()
    print('p',jobs)
